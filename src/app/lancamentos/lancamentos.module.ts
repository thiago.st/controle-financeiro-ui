import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { CurrencyMaskModule } from 'ng2-currency-mask';
import { InputTextareaModule } from 'primeng/components/inputtextarea/inputtextarea';
import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { ButtonModule } from 'primeng/components/button/button';
// import { DataTableModule } from 'primeng/components/datatable/datatable';
import {TableModule} from 'primeng/components/table/table';
import {TooltipModule} from 'primeng/components/tooltip/tooltip';
import {CalendarModule} from 'primeng/components/calendar/calendar';
import {SelectButtonModule} from 'primeng/components/selectbutton/selectbutton';
import {DropdownModule} from 'primeng/components/dropdown/dropdown';

import { LancamentoCadastroComponent } from './lancamento-cadastro/lancamento-cadastro.component';
// import { LancamentosGridComponent } from './lancamentos-grid/lancamentos-grid.component';
import { LancamentosPesquisaComponent } from './lancamentos-pesquisa/lancamentos-pesquisa.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [
    CommonModule,
    FormsModule,

    InputTextareaModule,
    InputTextModule,
    ButtonModule,
    // DataTableModule,
    TableModule,
    TooltipModule,
    CalendarModule,
    SelectButtonModule,
    DropdownModule,

    CurrencyMaskModule,

    SharedModule

    ],
    declarations: [
        LancamentoCadastroComponent,
        // LancamentosGridComponent,
        LancamentosPesquisaComponent
    ],

    exports: [
        LancamentoCadastroComponent,
        LancamentosPesquisaComponent
    ]
})
export class LancamentosModule { }
