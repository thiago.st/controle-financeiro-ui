import { Injectable } from '@angular/core';
import { Http, URLSearchParams } from '@angular/http';

export interface LancamentoFiltro {
    descricao: string;
}

@Injectable()
export class LancamentoService {

    lancamentosUrl = 'http://localhost:8000/apiLaravel/lancamentos/';

    constructor(private http: Http) { }

    pesquisar(filtro: LancamentoFiltro): Promise<any> {
        const params = new URLSearchParams();

        if (filtro.descricao) {
            params.set('descricao', filtro.descricao);
        }
        return this.http.get(this.lancamentosUrl, {search: params})
        .toPromise()
        .then(response => response.json().data);
    }

}
