import { Component, OnInit } from '@angular/core';
import { LancamentoService } from '../lancamento.service';

@Component({
    selector: 'app-lancamentos-pesquisa',
    templateUrl: './lancamentos-pesquisa.component.html',
    styleUrls: ['./lancamentos-pesquisa.component.css']
})
export class LancamentosPesquisaComponent implements OnInit {

    descricao: string;
    lancamentos = [];

    constructor(private lancamentoService: LancamentoService) {}

    ngOnInit() {
        this.pesquisar();
    }

    pesquisar() {
        console.log('entrou no metodo pesquisar');
        this.lancamentoService.pesquisar({ descricao: this.descricao })
        .then(_lancamentos => {
            this.lancamentos = _lancamentos;
            console.log(this.lancamentos);
        // .then(() => null);
        });
    }
}
