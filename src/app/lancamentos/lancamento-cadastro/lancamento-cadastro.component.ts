import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'app-lancamento-cadastro',
    templateUrl: './lancamento-cadastro.component.html',
    styleUrls: ['./lancamento-cadastro.component.css']
})
export class LancamentoCadastroComponent implements OnInit {

    tipos = [
        {label: 'Receita', value: 'RECEITA'},
        {label: 'Despesa', value: 'DESPESA'}
    ];

    categorias = [
        {label: 'Alimentação', value: 1},
        {label: 'Transporte', value: 2}
    ];

    pessoas = [
        {label: 'Thiago Gonçalves', value: 1},
        {label: 'Jaquelline Florêncio', value: 2},
        {label: 'Charles Júnior', value: 3}
    ];

    salvar(form: NgForm) {
        console.log(form);
    }

    constructor() { }

    ngOnInit() {
    }

}
