import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
    selector: 'app-cadastro-pessoa',
    templateUrl: './cadastro-pessoa.component.html',
    styleUrls: ['./cadastro-pessoa.component.css']
})
export class CadastroPessoaComponent {

    salvar(form: NgForm) {
        console.log(form);
    }

}
