import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-pessoas-pesquisa',
    templateUrl: './pessoas-pesquisa.component.html',
    styleUrls: ['./pessoas-pesquisa.component.css']
})
export class PessoasPesquisaComponent {

    pessoas = [
        {
            nome: 'Thiago', cidade: 'Campina Grande', estado: 'PB',
            ativo: true
        },
        {
            nome: 'Charles', cidade: 'Campina Grande', estado: 'PB',
            ativo: true
        },
        {
            nome: 'Tayrone', cidade: 'Campina Grande', estado: 'PB',
            ativo: false
        },
        {
            nome: 'Atylla', cidade: 'Campina Grande', estado: 'PB',
            ativo: true
        },
        {
            nome: 'Marcelo', cidade: 'Patos', estado: 'PB',
            ativo: false
        },
        {
            nome: 'Angelo', cidade: 'João Pessoa', estado: 'PB',
            ativo: true
        }
    ];
}
